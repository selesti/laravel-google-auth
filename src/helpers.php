<?php

if (!function_exists('google_auth_button')) {
    function google_auth_button()
    {
        if (config('selesti.google-auth.auth_enabled')) {
            return view('selesti-google-auth::google-button')
            ->with('googleAuthRoute', route('selesti-google-auth'))
            ->with('googleAuthHidden', config('selesti.google-auth.auth_hidden'))
            ->with('googleAuthClientID', config('selesti.google-auth.auth_key'))
            ->with('googleAuthCSRFToken', config('selesti.google-auth.auth_csrf_token'))
            ->with('googleAuthButtonText', config('selesti.google-auth.auth_button_text'))
            ->with('googleAuthKeySequence', config('selesti.google-auth.auth_key_sequence'))
            ->with('googleAuthLoggedInLocation', config('selesti.google-auth.auth_redirect_url'))->render();
        } else {
            return '';
        }
    }
}
