<?php

namespace Selesti\GoogleAuth;

use Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class GoogleAuthServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/selesti/google-auth.php' => config_path('selesti/google-auth.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/config/selesti/google-auth.php',
            'selesti.google-auth'
        );

        if (!config('selesti.google-auth.auth_enabled')) {
            return false;
        }

        $this->loadViewsFrom(__DIR__.'/resources/views', 'selesti-google-auth');

        require_once(__DIR__.'/helpers.php');

        $this->setupRoutes();
        $this->setupBladeDirectives();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function setupRoutes()
    {
        Route::post(
            config('selesti.google-auth.auth_route'),
            config('selesti.google-auth.auth_method')
        )->name('selesti-google-auth');
    }

    public function setupBladeDirectives()
    {
        Blade::directive('googleauthbutton', function () {
            return "<?php echo google_auth_button(); ?>";
        });
    }
}
