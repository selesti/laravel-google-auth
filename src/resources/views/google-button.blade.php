<!-- Google SSO -->

<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="google-signin-client_id" content="{!! $googleAuthClientID !!}">
<style>
    #google-auth-login {
        @if ($googleAuthHidden)
        display: none;
        @endif
    }
    #google-auth-login .abcRioButton {
        min-width: 180px !important;
    }
    #google-auth-login .abcRioButtonContents span:before {
        content: "{{ $googleAuthButtonText }} "
    }
</style>

<script type="text/javascript">

    var googleUser = null;

    var googleAuthFail = function (err) {
        console.error(err);
    };

    var googleAuthSuccess = function (_gU) {

        if (!googleUser) {
            googleUser = _gU;
        }

        if (!jQuery('#google-auth-login').data('clicked')) {
            return false;
        }

        var google_auth_token = googleUser.getAuthResponse().id_token;

        jQuery.post('{{ $googleAuthRoute }}', {
            google_auth_token: google_auth_token,
            _token: {{ $googleAuthCSRFToken }}
        }, function (r) {
            try {
                if (r.success) {
                    window.location = '{{ url( $googleAuthLoggedInLocation ) }}';
                } else {
                    if (r.exception) {
                        googleUser.disconnect();
                    }
                    if (r.message) {
                        alert(r.message);
                    } else {
                        console.error(r);
                        alert('Sorry something went wrong, please check the console and report it.');
                    }
                }
            } catch(e){
                console.error(e);
                alert('Sorry something went wrong, please check the console and report it.');
            }
        });
    };

    var markClicked = function(){
        jQuery('#google-auth-login').data('clicked', true);
    };

    @if ($googleAuthHidden)

    var seq = {{ json_encode($googleAuthKeySequence) }},
        marker = 0,
        matchPattern = function (e) {
            if (e.keyCode == seq[marker]) {
                marker++;
                if (marker == seq.length) {
                    jQuery(window).off('keydown', matchPattern)
                    jQuery('#google-auth-login').show();

                    if (googleUser) {
                        jQuery('#google-auth-login').data('clicked', true)
                        googleAuthSuccess(googleUser);
                    }

                    return true;
                }
            } else {
                marker = 0;
            }
        };

    window.addEventListener('load', function () {
        try {
            jQuery(window).on('keydown', matchPattern);
        } catch (e) {
            console.error(e);
            alert('Please make sure jQuery is included');
        }
    });
    @endif

</script>
<!-- End Google SSO -->

<div id="google-auth-login" class="g-signin2" data-onsuccess="googleAuthSuccess" data-onfailure="googleAuthFail" onclick="javascript:markClicked()"></div>
