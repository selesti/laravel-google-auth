<?php

/**
 * Recommend adding the following into your .env
 * GOOGLE_AUTH_KEY
 */

return [

    'auth_enabled' => true,

    'auth_key' => getenv('GOOGLE_AUTH_KEY', null),

    'auth_hidden' => false,

    'auth_key_sequence' => array(83, 83, 79),

    'auth_button_text' => '',

    'auth_redirect_url' => '/',

    'auth_csrf_token' => 'window.Laravel.csrfToken',

    'auth_map_organisation' => null,

    'auth_organisation_user' => null,

    'auth_guard' => null,

    'auth_route' => 'google-auth',

    'auth_model' => '\App\User',

    'auth_middleware' => 'web',

    'auth_method' => 'Selesti\GoogleAuth\Http\Controllers\GoogleAuthController@authenticate',

];
