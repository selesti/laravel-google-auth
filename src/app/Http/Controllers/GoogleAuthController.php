<?php

namespace Selesti\GoogleAuth\Http\Controllers;

use \Auth;
use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoogleAuthController extends Controller
{
    protected $guard = null;
    protected $authModel = null;
    protected $mapToUser = null;
    protected $mapFromOrganisation = null;
    protected $organisationUser = null;

    public function __construct()
    {
        return $this->middleware(config('selesti.google-auth.auth_middleware'));
    }

    public function authenticate(Request $request)
    {
        $this->guard = config('selesti.google-auth.auth_guard');
        $this->mapToUser = config('selesti.google-auth.auth_organisation_user');
        $this->authModel = config('selesti.google-auth.auth_model');
        $this->organisationUser = config('selesti.google-auth.auth_organisation_user');
        $this->mapFromOrganisation = config('selesti.google-auth.auth_map_organisation');

        $response = array(
            'success' => false,
            'message' => 'An unknown error occured.',
            'exception' => true,
            'data' => null
        );

        $authToken = $request->get('google_auth_token', null);

        if ($authToken) {
            $googleApiResponse = @file_get_contents('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$authToken);

            if ($googleApiResponse) {
                try {
                    $googleUser = json_decode($googleApiResponse);

                    if ($googleUser->email_verified) {
                        if ($this->mapFromOrganisation) {
                            if (isset($googleUser->hd) && $googleUser->hd == $this->mapFromOrganisation) {
                                $emailToUse = $this->mapToUser;
                            } else {
                                $emailToUse = $googleUser->email;
                            }
                        } else {
                            $emailToUse = $googleUser->email;
                        }

                        $authUser = $this->authModel::where('email', $emailToUse)->first();

                        if (!$authUser) {
                            $response['message'] = 'Couldnt find account for '.$emailToUse;
                        } else {
                            Auth::guard($this->guard)->login($authUser, true);
                            $response['message']   = 'Logged in as '.$emailToUse;
                            $response['success']   = true;
                            $response['exception'] = false;
                            $response['data'] = $authUser;
                        }
                    } else {
                        $response['message'] = 'You can only login with verified email accounts';
                    }
                } catch (Exception $e) {
                    $response['message'] = 'Couldnt read JSON returned by Google auth server';
                    $response['exception'] = $e;
                }
            } else {
                $response['message'] = 'Google auth server rejected the login token';
            }
        } else {
            $response['message'] = 'Auth provided no login token';
        }

        return $response;
    }
}
